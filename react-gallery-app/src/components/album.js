import React, { Component } from 'react';

class Album extends Component {
    render() {
        return (
            <li className="image">
                <p>{this.props.album.title}</p>
                <img src={this.props.album.url} alt={this.props.album.title} />
            </li>
        );
    }
}

export default Album;
