import React, { Component } from 'react';
import Album from './album';

class Gallery extends Component {
    render() {
        let albums;
        if (this.props.albums) {
            albums = this.props.albums.map(album => {
                return (
                    <Album key={album.id} album={album} />
                );
            });
        }
        return (
            <div className="gallery">
                <h1>Main Gallery</h1>
                <ul>
                    {albums}
                </ul>
            </div>
        );
    }
}

export default Gallery;
