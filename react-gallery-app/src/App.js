import React, { Component } from 'react';
import $ from 'jquery';
import Gallery from './components/gallery';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      albums: []
    };
  }

  getAlbums() {
    $.ajax({
      url: 'https://jsonplaceholder.typicode.com/photos?albumId=1',
      dataType: 'json',
      cache: false,
      success: function (data) {
        this.setState({ albums: data }, function () {
          console.log(this.state);
        });
      }.bind(this),
      error: function (xhr, status, error) {
        console.log(error);
      }
    });
  }

  componentWillMount() {
    this.getAlbums();
  }

  componentDidMount() {
    this.getAlbums();
  }

  render() {
    return (
      <div className="App">
        <Gallery albums={this.state.albums} />
      </div>
    );
  }
}

export default App;
